package br.com.ufg.twitter.model;

public class Tweet implements Comparable<Tweet>{
	
	private Long id;
	private Integer qtdRetweeters;
	
	public Tweet(Long id, Integer qtdRetweetrs){
		this.id = id;
		this.qtdRetweeters = qtdRetweetrs;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getQtdRetweeters() {
		return qtdRetweeters;
	}
	public void setQtdRetweeters(Integer qtdRetweeters) {
		this.qtdRetweeters = qtdRetweeters;
	}
	public int compareTo(Tweet t) {
		
		 if (this.qtdRetweeters < t.getQtdRetweeters()) {
	            return -1;
	        }
	        if (this.qtdRetweeters > t.getQtdRetweeters()) {
	            return 1;
	        }
		
		return 0;
	}

}
