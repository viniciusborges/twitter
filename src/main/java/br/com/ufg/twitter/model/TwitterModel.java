package br.com.ufg.twitter.model;

import twitter4j.RateLimitStatus;
import twitter4j.Twitter;

public class TwitterModel {
	
	private Twitter twitter;
	private Integer rateLimiteRetwitters;
	private Integer rateLimiteUserTimeline;
	private Long timestampRetwitter;
	private Long timestampUserTimeLine;
	private Integer rateLimiteFavorites;
	private Long timestampFavorites;
	
	
	public TwitterModel(Twitter twitter) throws Exception{
	
		RateLimitStatus rlsRetwitters = twitter.getRateLimitStatus("statuses").get("/statuses/retweeters/ids");
		this.timestampRetwitter = System.currentTimeMillis() - (rlsRetwitters.getSecondsUntilReset() * 1000);
		this.rateLimiteRetwitters = rlsRetwitters.getRemaining();
		
		RateLimitStatus rlsUserTimeline = twitter.getRateLimitStatus("statuses").get("/statuses/user_timeline");
		this.rateLimiteUserTimeline = rlsUserTimeline.getRemaining();
		this.timestampUserTimeLine = System.currentTimeMillis() - (rlsUserTimeline.getSecondsUntilReset() * 1000);
		
		RateLimitStatus rlsFavorites = twitter.getRateLimitStatus("favorites").get("/favorites/list");
		this.rateLimiteFavorites = rlsFavorites.getRemaining();
		this.timestampFavorites = System.currentTimeMillis() - (rlsFavorites.getSecondsUntilReset() * 1000);
		
		this.twitter = twitter;
	}

	public Long getTimestampRetwitter() {
		return timestampRetwitter;
	}

	public void setTimestampRetwitter(Long timestampRetwitter) {
		this.timestampRetwitter = timestampRetwitter;
	}

	public Long getTimestampUserTimeLine() {
		return timestampUserTimeLine;
	}

	public void setTimestampUserTimeLine(Long timestampUserTimeLine) {
		this.timestampUserTimeLine = timestampUserTimeLine;
	}

	public Twitter getTwitter() {
		return twitter;
	}

	public void setTwitter(Twitter twitter) {
		this.twitter = twitter;
	}

	public Integer getRateLimiteRetwitters() {
		return rateLimiteRetwitters;
	}

	public void setRateLimiteRetwitters(Integer rateLimiteRetwitters) {
		this.rateLimiteRetwitters = rateLimiteRetwitters;
	}

	public Integer getRateLimiteUserTimeline() {
		return rateLimiteUserTimeline;
	}

	public void setRateLimiteUserTimeline(Integer rateLimiteUserTimeline) {
		this.rateLimiteUserTimeline = rateLimiteUserTimeline;
	}

	public Integer getRateLimiteFavorites() {
		return rateLimiteFavorites;
	}

	public void setRateLimiteFavorites(Integer rateLimiteFavorites) {
		this.rateLimiteFavorites = rateLimiteFavorites;
	}

	public Long getTimestampFavorites() {
		return timestampFavorites;
	}

	public void setTimestampFavorites(Long timestampFavorites) {
		this.timestampFavorites = timestampFavorites;
	}

}
