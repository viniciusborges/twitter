package br.com.ufg.twitter.grafos;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import br.com.ufg.twitter.utils.TwitterUtils;

public class GrafosRetweets {
	
	public static void main(String[] args) throws Exception{
		
		String idUsuarioAlvo = "298687578";

		Map<Long, Map<Long, Integer>> grafo = new TreeMap<Long, Map<Long,Integer>>();
		Map<Long, Set<Long>> indiceInvertido = GrafosItemSet.getIndiceInvertido(idUsuarioAlvo);

		for(Long retweet : indiceInvertido.keySet()){
				GrafosItemSet.addGrafo(grafo, indiceInvertido.get(retweet), 1);
			}
		
		TwitterUtils.geraListaArestasSemMascara(grafo, idUsuarioAlvo);
		}
}
