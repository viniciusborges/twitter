package br.com.ufg.twitter.grafos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import br.com.ufg.twitter.model.Tweet;
import br.com.ufg.twitter.utils.TwitterUtils;

public class GrafosItemSet {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception{

		Map<Long, Map<Long, Integer>> grafo = new TreeMap<Long, Map<Long,Integer>>();
		Map<Long, Set<Long>> indiceInvertido = getIndiceInvertido("953158939");

		BufferedReader bfr = new BufferedReader(new FileReader("resultados/953158939/itemset_953158939.txt"));

		String linha = null;
		while((linha = bfr.readLine()) != null){

			String[] itemSetString = linha.split(" ");

			//Somente itemsets maiores ou igual 3, a ultima posicao e o suporte
			if(itemSetString.length >= 4){

				//Integer suporte = Integer.valueOf(itemSetString[itemSetString.length - 1].replace("(", "").replace(")", ""));
				List<Tweet> itemSet = getItemSetLongOrdenado(itemSetString, indiceInvertido);

				//Lista dos usuarios pertencentes ao itemset
				Set<Long> listaInterseccao = getListaInterseccao(itemSet, indiceInvertido);

				addGrafo(grafo, listaInterseccao, itemSet.size());
				
				//witterUtils.gravarListaInterseccao(listaInterseccao);
				//TwitterUtils.gravarItemSet(itemSetString);
			}
		}

		//Alternar entre gravar no arquivo e saida do cluto
		//TwitterUtils.gravarGrafoArquivoCluto(grafo);
		//TwitterUtils.gravarArquivosaidaCluto(grafo);
		//TwitterUtils.arquivoEntradaOSLOM(grafo);
		//TwitterUtils.saidaDemon(grafo);
		
	}


	//Peso da aresta e o tamanho do itemSet, quantidade de retweets em um itemSet
	public static void addGrafo(Map<Long, Map<Long, Integer>> grafo, Set<Long> listaInterseccao, Integer pesoAresta){

		for(Long vertice : listaInterseccao){

			if(!grafo.containsKey(vertice))
				grafo.put(vertice, new TreeMap<Long, Integer>());

			for(Long aresta : listaInterseccao){

				if(vertice != aresta){

					if(!grafo.get(vertice).containsKey(aresta))
						grafo.get(vertice).put(aresta, pesoAresta);

					else
						grafo.get(vertice).put(aresta, grafo.get(vertice).get(aresta) + pesoAresta);

				}

			}

		}

	}

	public static Set<Long> getListaInterseccao(List<Tweet> itemSet, Map<Long, Set<Long>> indiceInvertido){

		//Copia da lista invertida do primeiro tweet da ordenacao do ItemSet
		Set<Long> listaInterseccao = getCopiaLista(indiceInvertido.get(itemSet.get(0).getId()));

		for(int i = 1; i < itemSet.size(); i++){

			Set<Long> indiceInvertidoItemSet = indiceInvertido.get(itemSet.get(i).getId());

			List<Long> removerListaInterseccao = new ArrayList<Long>();
			for(Long usuario : listaInterseccao){

				if(!indiceInvertidoItemSet.contains(usuario)){
					removerListaInterseccao.add(usuario);
				}

			}

			//Somente para remover da lista de interseccao
			for(Long usuario : removerListaInterseccao){
				listaInterseccao.remove(usuario);
			}

		}

		return listaInterseccao;
	}

	public static Set<Long> getCopiaLista(Set<Long> original){

		Set<Long> copia = new TreeSet<Long>();

		for(Long tweet : original){
			copia.add(tweet);
		}

		return copia;
	}

	public static List<Tweet> getItemSetLongOrdenado(String[] itemSet, Map<Long, Set<Long>> indiceInvertido){

		List<Tweet> itemSetOrdenado = new ArrayList<Tweet>();

		//length - 1 pois a ultima posicao e o suporte
		for(int i = 0; i < (itemSet.length - 1); i++){

			Long tweetId = Long.valueOf(itemSet[i]);

			itemSetOrdenado.add(new Tweet(tweetId, indiceInvertido.get(tweetId).size()));

		}

		Collections.sort(itemSetOrdenado);

		return itemSetOrdenado;
	}

	@SuppressWarnings("resource")
	public static Map<Long, Set<Long>> getIndiceInvertido(String idUsuarioAlvo) throws Exception{
		Map<Long, Set<Long>> indiceInvertido = new TreeMap<Long, Set<Long>>();

		BufferedReader bfr = new BufferedReader(new FileReader("resultados/"+ idUsuarioAlvo +"/comunidade_"+ idUsuarioAlvo +".txt"));

		String linha = null;

		//Construindo Indice Invertido
		while((linha = bfr.readLine()) != null){
			Long[] retweets = getRetweetsLong(linha);

			//A posicao 0 sempre e de usuario
			for(int i = 1; i < retweets.length; i++){
				if(indiceInvertido.get(retweets[i]) != null){
					indiceInvertido.get(retweets[i]).add(retweets[0]);
				}

				else{
					indiceInvertido.put(retweets[i], new TreeSet<Long>());
					indiceInvertido.get(retweets[i]).add(retweets[0]);
				}
			}	
		}

		return indiceInvertido;
	}

	public static Long[] getRetweetsLong(String linha){

		String[] retweets = linha.split(" ");
		Long[] retweetsLong = new Long[retweets.length];

		int i = 0;
		for(String retweet : retweets){

			retweetsLong[i] = Long.valueOf(retweet);

			i++;
		}

		return retweetsLong;
	}



}