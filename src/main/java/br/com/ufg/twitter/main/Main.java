package br.com.ufg.twitter.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.ufg.twitter.utils.TwitterUtils;

public class Main {

	public static void main(String[] args) throws IOException {
		
		List<FileReader> lista = new ArrayList<FileReader>();
		lista.add(new FileReader("resultados/298687578/comunidade_298687578_1.txt"));
		lista.add(new FileReader("resultados/298687578/comunidade_298687578_2.txt"));
		lista.add(new FileReader("resultados/298687578/comunidade_298687578_3.txt"));
		
		TwitterUtils.uniArquivosComunidadesExtracao(lista, "298687578");
		
	}
	
}
