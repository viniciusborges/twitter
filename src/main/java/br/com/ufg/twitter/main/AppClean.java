package br.com.ufg.twitter.main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import br.com.ufg.twitter.utils.TwitterUtils;

public class AppClean {
	public static void main(String[] args) throws Exception, FileNotFoundException {

		BufferedReader retweetersArquivo = new BufferedReader(new FileReader("resultados/comunidade_2549447743_total.txt"));

		String linha = null;
		Set<String> usuarios = new HashSet<String>();
		while((linha = retweetersArquivo.readLine()) != null){
			String[] retweeterArray = linha.split(" ");
			Set<String> retweets = new HashSet<String>();

			if(retweeterArray.length > 1){

				if(usuarios.add(retweeterArray[0]))
					for(int i = 1; i < retweeterArray.length; i++){
						retweets.add(retweeterArray[i]);
					}
			}
			if(!retweets.isEmpty())
				TwitterUtils.gravarArquivo(retweeterArray[0], retweets, "2549447743_clean_comUsuario");
		}

		retweetersArquivo.close();
	}
}
