package br.com.ufg.twitter.main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;

import br.com.ufg.twitter.service.TwitterManager;
import br.com.ufg.twitter.utils.TwitterUtils;
import twitter4j.IDs;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;


public class App {
	public static void main( String[] args ) throws Exception{		

		Integer indice = 0;
		String usuarioAlvo = "";
		do{
			indice = Integer.parseInt(JOptionPane.showInputDialog("Indice maior que 0: "));
			usuarioAlvo = JOptionPane.showInputDialog("Usuario alvo: ");
		}
		while(indice <= 0 && usuarioAlvo.isEmpty());
		
		
		
		TwitterManager twitterManager = new TwitterManager();

		//Arquivo com os usuarios alvos da comunidade
		BufferedReader usuarioAlvoArquivo = new BufferedReader(new FileReader("resultados/"+ usuarioAlvo +"/usuario_alvo_"+ usuarioAlvo +".txt"));
		
		String linha = null;
		while((linha = usuarioAlvoArquivo.readLine()) != null){
			String[] retweetArray = linha.split(" ");

			//Arquivo onde e salva a comunidade. Caso o arquivo ja exista, carrega os usuarios no set para nao repetir
			Set<Long> usersRetweeters = TwitterUtils.getSetPreenchido("resultados/"+ usuarioAlvo +"/comunidade_" + usuarioAlvo + ".txt");

			//Usuario alvo da comunidade tambem pode aparecer nos 100 primeiros retweets
			usersRetweeters.add(Long.valueOf(retweetArray[0]));

			//Numero de retweets
			System.out.println(retweetArray.length);
			for(int i = indice; i < retweetArray.length; i++){

				System.out.println(i);
				IDs ids = null;

				try{
					ids = twitterManager.getRetweeterIds(Long.valueOf(retweetArray[i]), 200, -1);
				} catch (Exception e) {

					TwitterUtils.gravarLogErro(usuarioAlvo, retweetArray[i], i, e.getLocalizedMessage());
					if(e.getMessage().contains("400")){
						e.printStackTrace();
						System.out.println(retweetArray[i]);
						i--;
					}

					continue;
				}

				for(int j = 0; j < ids.getIDs().length; j++){

					Long id = ids.getIDs()[j];

					if(usersRetweeters.add(id)){

						//Contem Retweets
						Set<String> tweets = new HashSet<String>();

						Paging pagingRetweet = new Paging();
						pagingRetweet.setCount(200);

						for(int page = 1; page <= 160; page++){

							pagingRetweet.setPage(page);

							ResponseList<Status> responseRetweets = null;

							try {

								responseRetweets = twitterManager.getUserTimeline(id, pagingRetweet);

							} catch (Exception e) {
								
								TwitterUtils.gravarLogErro(usuarioAlvo, retweetArray[i], i, e.getLocalizedMessage());
								if(e.getMessage().contains("400")){
									e.printStackTrace();
									j--;
									System.out.println("\nUser: " + id);
								}

								continue;
							}


							if(responseRetweets.isEmpty())
								break;

							for(Status status : responseRetweets){
								if(status.isRetweet() && TwitterUtils.isIntersecao(retweetArray, String.valueOf(status.getRetweetedStatus().getId()))){
									tweets.add(String.valueOf(status.getRetweetedStatus().getId()));
								}
							}

						}

						TwitterUtils.gravarArquivo(id.toString(), tweets, retweetArray[0]);
					}
				}
				TwitterUtils.gravarLog(usuarioAlvo, retweetArray[i], i);
			}
		}
		usuarioAlvoArquivo.close();

	}


}
