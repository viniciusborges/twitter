package br.com.ufg.twitter.main;

import java.io.BufferedReader;
import java.io.FileReader;

import twitter4j.Status;
import twitter4j.Twitter;
import br.com.ufg.twitter.service.TwitterManager;

/**
 * 
 * @author Vinicius
 * 
 * App que imprime os tweets passados no arquivo "tweets.txt"
 *
 */

public class AppTweets {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		Twitter twitter = new TwitterManager().getInstanceAvailable("user_timeline").getTwitter();
		BufferedReader br = new BufferedReader(new FileReader("tweets.txt"));
		
		try {
		String linha = null;
		int i = 1;
		while((linha = br.readLine()) != null){
			
			String[] tweets = linha.split(" ");
			
			System.out.println("\n\n\nLinha " + i);
			
			for(String tweet : tweets){
				Status status = twitter.showStatus(Long.valueOf(tweet));
				
				if(status.getText() != null)
					System.out.println("Texto do tweet " + status.getId() + ":" + status.getText() + "\n");
			}
			i++;
		}
		} catch (Exception e) {
			if(e.getMessage().contains("420") || e.getMessage().contains("429") || e.getMessage().contains("88")){
				System.out.println("Por favor, espere 15 minutos e tente outra vez!!!");
				System.exit(0);
			}
			
			else{
				System.out.println("Ocorreu um erro, tente outra vez: ");
				System.out.println(e.getMessage());
				System.exit(0);
			}
		}
		
	}

}
