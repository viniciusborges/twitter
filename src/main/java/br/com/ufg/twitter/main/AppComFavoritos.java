package br.com.ufg.twitter.main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.ufg.twitter.service.TwitterManager;
import br.com.ufg.twitter.utils.TwitterUtils;
import twitter4j.IDs;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;


public class AppComFavoritos {
	public static void main( String[] args ) throws Exception{		

		TwitterManager twitterManager = new TwitterManager();

		//Arquivo com os usuarios alvos da comunidade
		BufferedReader retweetArquivo = new BufferedReader(new FileReader("resultados/65465465/usuario_alvo.txt"));

		String linha = null;
		while((linha = retweetArquivo.readLine()) != null){
			String[] retweetArray = linha.split(" ");

			//Arquivo onde e salva a comunidade. Caso o arquivo ja exista, carrega os usuarios no set para nao repetir
			Set<Long> usersRetweeters = TwitterUtils.getSetPreenchido("resultados/64654654/comunidade_" + retweetArray[0] + ".txt");

			//Usuario alvo da comunidade tambem pode aparecer nos 100 primeiros retweets
			usersRetweeters.add(Long.valueOf(retweetArray[0]));

			//Numero de retweets e favoritos do usuario alvo
			System.out.println(retweetArray.length);
			for(int i = 227; i < retweetArray.length; i++){

				System.out.println(i);
				IDs ids = null;

				try{
					ids = twitterManager.getRetweeterIds(Long.valueOf(retweetArray[i]), 200, -1);
				} catch (Exception e) {

					if(e.getMessage().contains("400")){
						e.printStackTrace();
						System.out.println(retweetArray[i]);
						i--;
					}

					continue;
				}

				for(int j = 0; j < ids.getIDs().length; j++){

					Long id = ids.getIDs()[j];

					if(usersRetweeters.add(id)){

						//Contem Retweets e Favoritos
						Set<String> tweets = new HashSet<String>();

						Paging pagingRetweet = new Paging();
						Paging pagingFavorite = new Paging();
						pagingFavorite.setCount(200);
						pagingRetweet.setCount(200);

						boolean favoritesIsEmpty = false;
						for(int page = 1; page <= 160; page++){

							pagingFavorite.setPage(page);
							pagingRetweet.setPage(page);

							ResponseList<Status> responseRetweets = null;
							ResponseList<Status> responseFavorites = null;

							try {

								responseRetweets = twitterManager.getUserTimeline(id, pagingRetweet);

								//Fazer somente ate 15 requisicoes por usuario para os favoritos
								if(!favoritesIsEmpty && page <= 15)
									responseFavorites = twitterManager.getFavorites(id, pagingFavorite);

							} catch (Exception e) {

								if(e.getMessage().contains("400")){
									e.printStackTrace();
									j--;
									System.out.println("\nUser: " + id);
								}

								continue;
							}

							if(responseFavorites == null || responseFavorites.isEmpty())
									favoritesIsEmpty = true;
							

							if((responseFavorites == null || responseFavorites.isEmpty()) && responseRetweets.isEmpty())
								break;

							for(Status status : responseRetweets){
								if(status.isRetweet() && TwitterUtils.isIntersecao(retweetArray, String.valueOf(status.getRetweetedStatus().getId()))){
									tweets.add(String.valueOf(status.getRetweetedStatus().getId()));
								}
							}

							if(responseFavorites != null)
								for(Status status : responseFavorites){
									//Interseccao e tweets q ele favorita e nao sao deles
									if(TwitterUtils.isIntersecao(retweetArray, String.valueOf(status.getId())) && status.getUser().getId() != id){
										tweets.add(String.valueOf(status.getId()));
									}
								}
						}

						TwitterUtils.gravarArquivo(id.toString(), tweets, retweetArray[0]);
					}
				}

			}
		}
		retweetArquivo.close();

	}


}
