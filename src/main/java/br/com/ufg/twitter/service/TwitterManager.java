package br.com.ufg.twitter.service;

import java.util.Queue;

import br.com.ufg.twitter.model.TwitterModel;
import twitter4j.IDs;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;

public class TwitterManager {

	private Queue<TwitterModel> twitterFila;
	public final Integer RATE_LIMIT_RETWITTERS = 15;
	public final Integer RATE_LIMIT_USER_TIMELINE = 180;
	public final Integer RATE_LIMIT_FAVORITES = 15;

	public TwitterManager() throws Exception{
		twitterFila = new TwitterManyFactory().getTwitters();
	}

	public TwitterModel getInstanceAvailable(String endpoint) throws Exception{

		TwitterModel tm = null;

		if(endpoint.endsWith("user_timeline")){
			
			for(int i = 0; i < twitterFila.size(); i++){

				tm = twitterFila.peek();
				
				if(tm.getRateLimiteUserTimeline() > 0){
					tm.setRateLimiteUserTimeline(tm.getRateLimiteUserTimeline() - 1);
					break;
				}
				
				else if (((System.currentTimeMillis() - tm.getTimestampUserTimeLine())/1000) >= 900){
					tm.setRateLimiteUserTimeline(RATE_LIMIT_USER_TIMELINE - 1);
					break;
				}
				
				else if(i == (twitterFila.size() - 1)){
					i = 0;
				}
				
				else{
					tm = twitterFila.poll();
					twitterFila.add(tm);
				}
				
			}
			
		}
		
		else if(endpoint.endsWith("ids")){

			for(int i = 0; i < twitterFila.size(); i++){

				tm = twitterFila.peek();
				
				if(tm.getRateLimiteRetwitters() > 0){
					tm.setRateLimiteRetwitters(tm.getRateLimiteRetwitters() - 1);
					
					break;
				}
				
				else if (((System.currentTimeMillis() - tm.getTimestampRetwitter())/1000) >= 900){
					tm.setRateLimiteRetwitters(RATE_LIMIT_RETWITTERS - 1);
					break;
				}
				
				else if(i == (twitterFila.size() - 1)){
					i = 0;
				}
				
				else{
					tm = twitterFila.poll();
					twitterFila.add(tm);
				}
				
			}
		}
		
		else if(endpoint.endsWith("favorites")){

			for(int i = 0; i < twitterFila.size(); i++){

				tm = twitterFila.peek();
				
				if(tm.getRateLimiteFavorites() > 0){
					tm.setRateLimiteFavorites(tm.getRateLimiteFavorites() - 1);
					
					break;
				}
				
				else if (((System.currentTimeMillis() - tm.getTimestampFavorites())/1000) >= 900){
					tm.setRateLimiteFavorites(RATE_LIMIT_FAVORITES - 1);
					break;
				}
				
				else if(i == (twitterFila.size() - 1)){
					i = 0;
				}
				
				else{
					tm = twitterFila.poll();
					twitterFila.add(tm);
				}
				
			}
		}

		return tm;
	}

	public IDs getRetweeterIds(Long idRetweet, int count, int cursor) throws Exception{

		TwitterModel tm = this.getInstanceAvailable("ids");
		
		IDs ids = tm.getTwitter().getRetweeterIds(idRetweet, count, cursor);
		
		if(tm.getRateLimiteRetwitters() == 0)
			tm.setTimestampRetwitter(System.currentTimeMillis());

		return ids;
	}

	public ResponseList<Status> getUserTimeline(Long userId, Paging paging) throws Exception{
		
		TwitterModel tm = this.getInstanceAvailable("user_timeline");
		ResponseList<Status> timeline = tm.getTwitter().getUserTimeline(userId, paging);
		
		if(tm.getRateLimiteUserTimeline() == 0)
			tm.setTimestampUserTimeLine(System.currentTimeMillis());
		
		return timeline;
	}
	
	public ResponseList<Status> getFavorites(Long userId, Paging paging) throws Exception{
		
		TwitterModel tm = this.getInstanceAvailable("favorites");
		ResponseList<Status> favorites = tm.getTwitter().getFavorites(userId, paging);
		
		if(tm.getRateLimiteFavorites() == 0)
			tm.setTimestampFavorites(System.currentTimeMillis());
		
		return favorites;
		
	}

}
