package br.com.ufg.twitter.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import br.com.ufg.twitter.service.TwitterManager;


public class TwitterUtils {

	//Imprime as comunidades presentes na saida do Demon
	public static void saidaDemon(Map<Long, Map<Long, Integer>> grafo) throws IOException{

		PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/953158939/DEMON/saidaDEMON_953158939.txt", true));
		BufferedReader bfr = new BufferedReader(new FileReader("resultados/953158939/DEMON/communities"));

		//Lista invertida da mapIdNumero
		Map<Integer, Long> mapNumeroId = new TreeMap<Integer, Long>();

		int i = 1;
		for(Long vertice : grafo.keySet()){
			mapNumeroId.put(i, vertice);
			i++;
		}

		String linha = "";
		while((linha = bfr.readLine()) != null){
			String[] comunidade = linha.split("\t")[1].split(",");

			String saida = "";
			for(String idMascara : comunidade){
				saida += mapNumeroId.get(Integer.parseInt(idMascara)).toString() + " ";
			}
			saida += "\n";
			gravarArq.printf(saida);
		}

		gravarArq.close();


	}

	//Gera arquivo de entrada para o Cluto
	public static void gravarGrafoArquivoCluto(Map<Long, Map<Long, Integer>> grafo) throws IOException{

		PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/entradaCluto_2549447743.txt", true));

		//Map com o ID original e o numero que o representa
		Map<Long, Integer> mapIdNumero = new TreeMap<Long, Integer>();

		//Lista invertida da mapIdNumero
		Map<Integer, Long> mapNumeroId = new TreeMap<Integer, Long>();

		Integer totalArestas = 0;
		int i = 1;

		for(Long vertice : grafo.keySet()){
			totalArestas += grafo.get(vertice).size();
			mapIdNumero.put(vertice, i);
			mapNumeroId.put(i, vertice);
			i++;
		}

		String grafoString = grafo.keySet().size() + " " + totalArestas + "\n";
		int x = 0;
		for(Long vertice : grafo.keySet()){

			//grafoString += mapIdNumero.get(vertice);

			for(Long aresta : grafo.get(vertice).keySet()){
				grafoString += " " + mapIdNumero.get(aresta) + " " + (float) grafo.get(vertice).get(aresta);
			}

			grafoString += "\n";
			x++;
		}

		gravarArq.printf(grafoString);

		gravarArq.close();


	}

	//Gera arquivo de entrada para o OSLOM
	public static void arquivoEntradaOSLOM(Map<Long, Map<Long, Integer>> grafo) throws IOException{

		PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/953158939/lista_edges_953158939.txt", true));

		//Map com o ID original e o numero que o representa
		Map<Long, Integer> mapIdNumero = new TreeMap<Long, Integer>();

		//Lista invertida da mapIdNumero
		Map<Integer, Long> mapNumeroId = new TreeMap<Integer, Long>();

		int i = 1;
		for(Long vertice : grafo.keySet()){
			mapIdNumero.put(vertice, i);
			mapNumeroId.put(i, vertice);
			i++;
		}

		String grafoString = "";
		int x = 0;
		for(Long vertice : grafo.keySet()){

			for(Long aresta : grafo.get(vertice).keySet()){
				grafoString += mapIdNumero.get(vertice) + " " + mapIdNumero.get(aresta);
				grafoString += "\n";
			}

			x++;
		}

		gravarArq.printf(grafoString);

		gravarArq.close();

	}

	public static void geraListaArestasSemMascara(Map<Long, Map<Long, Integer>> grafo, String idUsuarioAlvo) throws IOException{
		PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/"+ idUsuarioAlvo +"/lista_edges_"+ idUsuarioAlvo +".txt", true));

		String grafoString = "";
		int x = 0;
		for(Long vertice : grafo.keySet()){

			for(Long aresta : grafo.get(vertice).keySet()){
				grafoString += vertice + " " + aresta + " " + grafo.get(vertice).get(aresta);
				grafoString += "\n";
			}

			x++;
		}

		gravarArq.printf(grafoString);

		gravarArq.close();
	}


	//Imprime os vertices do grafo, de acordo com a saida do cluto
	public static void gravarArquivosaidaCluto(Map<Long, Map<Long, Integer>> grafo) throws Exception{

		Map<Integer, List<Long>> clusters = new HashMap<Integer, List<Long>>();
		List<Long> vertices = new ArrayList<Long>();
		List<Integer> clustersNumero = new ArrayList<Integer>();
		BufferedReader bfr = new BufferedReader(new FileReader("resultados/entradaCluto.txt.clustering.9"));


		int i = 1;
		for(Long vertice : grafo.keySet()){
			vertices.add(vertice);
		}

		String linha = null;
		while((linha = bfr.readLine()) != null){
			clustersNumero.add(Integer.valueOf(linha.trim()));
		}

		for(int j = 0; j < clustersNumero.size(); j++){

			if(!clusters.containsKey(clustersNumero.get(j))){
				clusters.put(clustersNumero.get(j), new ArrayList<Long>());
			}

			clusters.get(clustersNumero.get(j)).add(vertices.get(j));

		}

		String saida = "";
		for(Integer cluster : clusters.keySet()){

			saida+="Numero cluster: " + cluster + "\n";

			for(Long vertice : clusters.get(cluster)){

				saida += vertice;
				saida += "\n";

			}

			saida +="\n\n";
		}

		PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/clusters.txt", true));

		gravarArq.printf(saida);

		gravarArq.close();

	}

	public static void gravarListaInterseccao(Set<Long> listaInterseccao) throws IOException{

		PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/953158939/listaInterseccao_953158939.txt", true));

		String saida = listaInterseccao.toString().replace("[", "").replace("]", "").replace(",", "") + "\n";

		gravarArq.printf(saida);

		gravarArq.close();


	}

	public static void gravarItemSet(String[] itemSetString) throws IOException{

		PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/saidaMaximal_1005839072_MaiorQue3.txt", true));
		String saida="";

		for(String item : itemSetString){
			saida+= item + " ";
		}
		saida += "\n";
		gravarArq.printf(saida);

		gravarArq.close();


	}

	public static void gravarArquivo(String id, Set<String> idsRetweets, String idUserComunidade) throws IOException{

		if(idsRetweets != null && !idsRetweets.isEmpty()){
			PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/"+ idUserComunidade +"/comunidade_" + idUserComunidade + ".txt", true));

			String linha = "";
			for(String retweet : idsRetweets){
				linha += " " + retweet;
			}

			gravarArq.printf(id + linha + "%n");
			//gravarArq.printf(linha.trim() + "%n");

			gravarArq.close();

		}

	}


	public static void gravarArquivo(Set<String> idsRetweets, String idUserComunidade) throws IOException{
		gravarArquivo("", idsRetweets, idUserComunidade);
	}

	public static void gravarUsuarioAlvo(String[] usuarioAlvo) throws IOException{

		Set<String> retweetsUsuarioAlvo = new HashSet<String>();

		for (int i = 1; i < usuarioAlvo.length; i++) {
			retweetsUsuarioAlvo.add(usuarioAlvo[i]);
		}

		gravarArquivo(usuarioAlvo[0], retweetsUsuarioAlvo, usuarioAlvo[0]);

	}

	public static Set<Long> getSetPreenchido(String arquivoPath) throws Exception{

		Set<Long> usersRetweeters = new HashSet<Long>();

		if(new File(arquivoPath).exists()){
			BufferedReader comunidadeArquivo = new BufferedReader(new FileReader(arquivoPath));
			String linha = null;

			while((linha = comunidadeArquivo.readLine()) != null){
				usersRetweeters.add(Long.valueOf(linha.split(" ")[0]));
			}

			comunidadeArquivo.close();
		}


		return usersRetweeters;

	}

	public static boolean isIntersecao(String[] retweets, String idRetweet){

		for(String retweet : retweets){
			if(retweet.equals(idRetweet))
				return true;
		}

		return false;
	}

	public static void gerarTimeline(String idUsuario){

		try {
			PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/"+ idUsuario +"/usuario_alvo_953158939.txt", true));
			gravarArq.printf(idUsuario);
			TwitterManager twitterManager = new TwitterManager();

			List<String> tweets = new ArrayList<String>();

			Paging pagingRetweet = new Paging();
			pagingRetweet.setCount(200);

			for(int page = 1; page <= 160; page++){

				pagingRetweet.setPage(page);

				ResponseList<Status> responseRetweets = twitterManager.getUserTimeline(Long.valueOf(idUsuario), pagingRetweet);

				if(responseRetweets.isEmpty())
					break;

				for(Status status : responseRetweets){
					if(status.isRetweet()){
						tweets.add(String.valueOf(status.getRetweetedStatus().getId()));
					}
				}

			}

			for(String tweet : tweets)
				gravarArq.printf(" " + tweet);

			gravarArq.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void uniArquivosComunidadesExtracao(List<FileReader> arquivos, String idUsuarioAlvo) throws IOException{

		Map<String, String[]> uniaoComunidade = new TreeMap<String, String[]>();
		PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/"+ idUsuarioAlvo +"/comunidade_"+ idUsuarioAlvo +".txt", true));

		for(FileReader arquivo : arquivos){
			BufferedReader bfrArquivo = new BufferedReader(arquivo);

			String linha="";
			while((linha = bfrArquivo.readLine()) != null){
				String linhaVetor[] = linha.split(" ");
				uniaoComunidade.put(linhaVetor[0], linhaVetor);
			}
		}

		String arquivo = "";
		int i = 0;
		System.out.println(uniaoComunidade.keySet().size() + "\n");
		for(String id : uniaoComunidade.keySet()){
			for(String retweet : uniaoComunidade.get(id)){
				arquivo += retweet + " "; 
			}
			arquivo = arquivo.trim();
			arquivo += "\n";
			i++;
			System.out.println(i + "\n");
		}

		gravarArq.write(arquivo);
		gravarArq.close();

	}

	public static void removeUsuarioExtracao(FileReader file, String idUsuarioAlvo) throws IOException{

		BufferedReader bfrArquivo = new BufferedReader(file);
		PrintWriter gravarArq = new PrintWriter(new FileWriter("resultados/"+ idUsuarioAlvo +"/comunidade_"+ idUsuarioAlvo +"_semUsuario.txt", true));

		String arquivo = "";
		String linha = "";
		while((linha = bfrArquivo.readLine()) != null){
			int i = 0;
			for(String retweet : linha.split(" ")){
				if(i != 0){
					arquivo += retweet + " ";
				}
				i++;
			}
			arquivo += "\n";
		}

		gravarArq.write(arquivo);
		gravarArq.close();
	}

	public static void gravarLog(String usuarioAlvo, String retweet, int indice) throws IOException{
		PrintWriter gravarLog = new PrintWriter(new FileWriter("resultados/"+ usuarioAlvo +"/log_"+ usuarioAlvo +".txt", true));

		gravarLog.write("Retweet: " +  retweet + ", Indice: " + indice + "\n");

		gravarLog.close();
	}

	public static void gravarLogErro(String usuarioAlvo, String retweet, int indice, String erro) throws IOException{

		PrintWriter gravarLogErro = new PrintWriter(new FileWriter("resultados/"+ usuarioAlvo +"/log_erro_"+ usuarioAlvo +".txt", true));

		gravarLogErro.write("Retweet: " +  retweet + ", Indice: " + indice + "\n" + erro + "\n\n");

		gravarLogErro.close();

	}

}
